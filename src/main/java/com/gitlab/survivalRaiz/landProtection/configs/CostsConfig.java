package com.gitlab.survivalRaiz.landProtection.configs;

import api.skwead.storage.file.yml.YMLConfig;
import com.gitlab.survivalRaiz.landProtection.terrain.Terrain;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class CostsConfig extends YMLConfig {

    public CostsConfig(JavaPlugin plugin) {
        super("costs", plugin);

	final Set<String> headers = new HashSet<>();

        try {
            headers.add("chunk");
            headers.add("tax");
            headers.add("min_size_tax");

            headers.add("time_before_payment");

            this.createConfig(headers);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void createConfig(Set<String> set) throws IOException {
        for (String param : set) {
            if (super.getConfig().get(param) == null) {
                super.getConfig().createSection(param);
                super.getConfig().set(param, 0);
                super.getConfig().save(super.getFile());
            }
        }
    }

    public double getChunkPrice() {
        return super.getConfig().getDouble("chunk");
    }

    public double getTax() {
        return super.getConfig().getDouble("tax");
    }

    public int getMinTaxSize() {
        return super.getConfig().getInt("min_size_tax");
    }

    public int getTimeThreshold() {
        return super.getConfig().getInt("time_before_payment");
    }

    public double calculate(Terrain t) {
        final double tax = getTax();
        final int r = t.getR();
        final int min = getMinTaxSize();

        return r <= min ? 0 : tax * r;
    }
}
