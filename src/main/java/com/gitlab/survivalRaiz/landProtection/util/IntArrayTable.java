package com.gitlab.survivalRaiz.landProtection.util;

import java.util.Arrays;

/**
 * A generalized data structure to map single integer keys to multiple integer values.
 * Only supports integers to be slightly faster than the generics implementation {@link ArrayTable}
 * This is similar to a HashTable, but stores the data in arrays for the fastest possible search.
 * Does not support duplicated key value pairs.
 * Insertion is slower due to array expansion.
 * All search implements binary search, and the arrays are dimensioned dynamically and sorted at insertion and removal.
 */
public class IntArrayTable {
    private int[] keys = new int[0];
    private final int[][] values = new int[0][0];

    public int[] get(int key) {
        final int i = Arrays.binarySearch(keys, key);
        if (i < 0) return null;
        return values[i];
    }

    public boolean contains(int key, int value) {
        return Arrays.binarySearch(get(key), value) > 0; // TODO: 8/7/20 vêr se isto pode dar NPE
    }

    public void insert(int key, int value) {
        if (contains(key, value)) return;

        if (get(key) == null) {
            int[] k = new int[this.keys.length + 1];
            System.arraycopy(this.keys, 0, k, 0, keys.length);
            k[k.length - 1] = key;
            this.keys = k;
            sort();
        }

        int[] vs = this.get(key);
        int[] v = new int[vs.length + 1];
        System.arraycopy(vs, 0, v, 0, vs.length);
        vs = v;
        sort(vs);
    }

    public void remove(int key, int value){
        int[] v = get(key);
        int[] n = new int[v.length - 1];

        for (int i = 0; i < v.length; i++) {
            if (v[i] == value)
                while (i < v.length-1)
                    v[i] = v[++i];
        }

        System.arraycopy(v, 0, n, 0, v.length-1);
        v = n;
    }

    private void sort(int[] arr) {
        int n = arr.length;
        for (int i = 1; i < n; ++i) {
            int key = arr[i];
            int j = i - 1;

            while (j >= 0 && arr[j] > key) {
                arr[j + 1] = arr[j];
                j = j - 1;
            }
            arr[j + 1] = key;
        }
    }

    private void sort() {
        int n = keys.length;
        for (int j = 1; j < n; j++) {
            int key = keys[j];
            int[] value = values[j];
            int i = j - 1;
            while (i > -1 && keys[i] > key) {
                keys[i + 1] = keys[i];
                values[i + 1] = values[i];
                i--;
            }
            keys[i + 1] = key;
            values[i + 1] = value;
        }
    }

    @Override
    public String toString() {
        return "IntArrayTable{" +
                "keys=" + Arrays.toString(keys) +
                ", values=" + Arrays.toString(values) +
                '}';
    }
}
