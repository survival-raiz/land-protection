package com.gitlab.survivalRaiz.landProtection;

import api.skwead.commands.CommandManager;
import api.skwead.commands.ConfigCommand;
import com.gitlab.survivalRaiz.core.Core;
import com.gitlab.survivalRaiz.economy.EconomyPlugin;
import com.gitlab.survivalRaiz.landProtection.commands.*;
import com.gitlab.survivalRaiz.landProtection.configs.CostsConfig;
import com.gitlab.survivalRaiz.landProtection.events.MoveEvt;
import com.gitlab.survivalRaiz.landProtection.terrain.LandManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class LandProtection extends JavaPlugin {

    private Core core;
    private EconomyPlugin economy;

    private LandManager landManager;

    private CostsConfig costsConfig;

    @Override
    public void onEnable() {

        core = (Core) this.getServer().getPluginManager().getPlugin("Core");
        if (core == null) return; // TODO: 8/18/20 Melhorar este error handling

        landManager = new LandManager(core.getDbManager(), this);

        economy = (EconomyPlugin) this.getServer().getPluginManager().getPlugin("EconomyPlugin");

        final CommandManager cm = new CommandManager(this);

        try {
            final Set<ConfigCommand> cmds = new HashSet<>();
            cmds.add(new ClaimCmd(this));
            cmds.add(new PayTaxesCmd(this));
            cmds.add(new TransferCmd(this));
            cmds.add(new ListDueTerrainsCmd(this));

            cm.getConfig().generateFrom(cmds);
        } catch (IOException e) {
            e.printStackTrace();
        }

        new MoveEvt(this, this.landManager);

        costsConfig = new CostsConfig(this);
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

    public LandManager getLandManager() {
        return landManager;
    }

    public CostsConfig getCostsConfig() {
        return this.costsConfig;
    }

    public EconomyPlugin getEconomyPlugin() {
        return economy;
    }

    public Core getCore() {
        return core;
    }
}
