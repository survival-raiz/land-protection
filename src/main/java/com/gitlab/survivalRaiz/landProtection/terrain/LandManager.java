package com.gitlab.survivalRaiz.landProtection.terrain;

import com.gitlab.survivalRaiz.core.db.DBManager;
import com.gitlab.survivalRaiz.economy.shop.Economy;
import com.gitlab.survivalRaiz.landProtection.LandProtection;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.*;

public class LandManager {
    private final DBManager dbManager;

    private final Set<Long> terrains = new HashSet<>();
    private final Set<Terrain> due = new HashSet<>();
    private int count = 0;

    public LandManager(DBManager dbManager, LandProtection plugin) {
        this.dbManager = dbManager;

        dbManager.fetchAllTable("terrains",
                (resultSet -> {
                    try {
                        while (resultSet.next()) {
                            final int x = resultSet.getInt(2);
                            final int y = resultSet.getInt(3);
                            final int r = resultSet.getInt(4);

                            if (r == 0)
                                this.terrains.add(((long) x << (4 * 8) | (long) y));
                            else
                                for (int i = x - r; i <= x + r; i++)
                                    for (int j = y - r; j <= y + r; j++)
                                        this.terrains.add(((long) i << (4 * 8) | (long) j));

//                            final LocalDate lastPay = LocalDateTime.parse(resultSet.getString(6)).toLocalDate();
                            final Terrain t = new Terrain(resultSet.getInt(2),
                                    resultSet.getInt(3),
                                    resultSet.getInt(4),
                                    UUID.fromString(resultSet.getString(5)),
                                    resultSet.getInt(1));

                            if (isDue(plugin, /*lastPay,*/ t))
                                due.add(t);
                            
                            this.count++;
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    } catch (NullPointerException e) {
                        System.out.println("A tabela não tem terrenos.");
                    }

                    return null;
                }));
    }

    public boolean isDue(LandProtection plugin, Terrain t) {
        final LocalDate lastPay = getDueDate(t.getId());

        if (LocalDate.now().minusMonths(1).isAfter(lastPay))
            return true;
        else if (LocalDate.now().minusMonths(1).equals(lastPay)) {
            final Economy economy = plugin.getEconomyPlugin().getEconomy();
            final OfflinePlayer p = Bukkit.getOfflinePlayer(t.getOwner());
            final double tax = plugin.getCostsConfig().calculate(t);

            if (economy.has(p.getUniqueId(), tax))
                economy.withdrawPlayer(p.getUniqueId(), tax);
            return true;
        }

        return false;
    }

    public LocalDate getDueDate(int id) {
        final List<Object> due = dbManager.getFromDb("terrains", "terrainId", id, resultSet -> {
            try {
                return Collections.singletonList(LocalDate.parse(resultSet.getString(6)));
            } catch (SQLException throwable) {
                throwable.printStackTrace();
            }

            return null;
        });

        return (LocalDate) due.get(0);
    }

    public boolean isTerrain(int x, int y) {
        return terrains.contains((long) x << (4 * 8) | (long) y);
    }

    public Terrain fromDb(int id) {
        final List<Object> raw = this.dbManager.getFromDb("terrains", "terrainId", id,
                (resultSet -> {
                    try {
                        final Terrain t = new Terrain(resultSet.getInt(2),
                                resultSet.getInt(3),
                                resultSet.getInt(4),
                                UUID.fromString(resultSet.getString(5)),
                                resultSet.getInt(1));

                        return Collections.singletonList(t);
                    } catch (SQLException e) {
                        e.printStackTrace();
                        return null;
                    }
                }));

        return (Terrain) raw.get(0);
    }

    public Set<Terrain> fromUser(UUID player) {
        final Set<Terrain> ts = new HashSet<>();
        this.dbManager.getTerrainsByPlayer(player,
                (resultSet -> {
                    try {
                        while (resultSet.next())
                            ts.add(new Terrain(resultSet.getInt(2),
                                    resultSet.getInt(3),
                                    resultSet.getInt(4),
                                    UUID.fromString(resultSet.getString(5)),
                                    resultSet.getInt(1)));
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                    return null;
                }));

        return ts;
    }

    public void register(Terrain t) {
        final int r = t.getR();
        final int x = t.getX();
        final int y = t.getY();

        if (r == 0)
            this.terrains.add((long) x << (4 * 8) | (long) y);
        else
            for (int i = x - r; i <= x + r; i++)
                for (int j = y - r; j <= y + r; j++)
                    this.terrains.add((long) i << 4 | j);

        this.dbManager.addTerrain(x, y, r, t.getOwner(), this.count++);
    }

    public Set<Terrain> getDue() {
        return due;
    }

    public int getCount() {
        return count;
    }
}
