package com.gitlab.survivalRaiz.landProtection.terrain;

import org.bukkit.Location;

import java.util.UUID;

public class Terrain {
    private final int x;
    private final int y;
    private final int r;
    private UUID owner;
    private int id;

    public Terrain(int x, int y, int r, UUID owner, int id) {
        this.x = x;
        this.y = y;
        this.r = r;
        this.owner = owner;
        this.id = id;
    }

    public Terrain(int x, int y, UUID owner, int r) {
        this(x, y, r, owner, 0);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public UUID getOwner() {
        return owner;
    }

    public int getId() {
        return id;
    }

    public int getR() {
        return this.r;
    }

    public void setId(int count) {
        this.id = count;
    }

    public void setOwner(UUID owner) {
        this.owner = owner;
    }

    public boolean contains(Location l) {
        final int x = l.getChunk().getX();
        final int y = l.getChunk().getZ();

        return this.x-r <= x && this.y-r <= y && this.x+r >= x && this.y+r >= y;
    }

    @Override
    public String toString() {
        return "Terrain{" +
                "x=" + x +
                ", y=" + y +
                ", r=" + r +
                ", owner=" + owner +
                ", id=" + id +
                '}';
    }
}
