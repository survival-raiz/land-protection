package com.gitlab.survivalRaiz.landProtection.commands;

import api.skwead.commands.ConfigCommand;
import api.skwead.exceptions.exceptions.CommandException;
import com.gitlab.survivalRaiz.core.excepions.SRCommandException;
import com.gitlab.survivalRaiz.core.messages.Message;
import com.gitlab.survivalRaiz.landProtection.LandProtection;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Collections;

public class ListCmd extends ConfigCommand {

    private final LandProtection plugin;

    public ListCmd(LandProtection plugin) {
        super("terrenos", "Mostra os terrenos do jogador",
                "/terrenos", Collections.singletonList("tl"), "terrenos");
        this.plugin = plugin;
    }

    @Override
    public int validate(CommandSender commandSender, String s, String[] strings) throws CommandException {
        if (!(commandSender instanceof Player))
            throw new SRCommandException(commandSender, Message.PLAYER_ONLY_COMMAND, plugin.getCore());

        return 0;
    }

    @Override
    public void run(CommandSender commandSender, String s, String[] strings) throws CommandException {
        validate(commandSender, s, strings);

        final Player p = (Player) commandSender;

        // TODO: 10/2/20 Integrar com a API de mensagens
        plugin.getLandManager().fromUser(p.getUniqueId()).forEach(terrain -> {
            final String msg = "[" + terrain.getId() + "] em (" + terrain.getX() + " , " + terrain.getY() + ")";

            p.sendMessage(msg);
        });
    }
}
