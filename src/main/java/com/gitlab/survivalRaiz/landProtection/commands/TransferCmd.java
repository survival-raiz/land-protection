package com.gitlab.survivalRaiz.landProtection.commands;

import api.skwead.commands.ConfigCommand;
import api.skwead.exceptions.exceptions.CommandException;
import com.gitlab.survivalRaiz.core.excepions.SRCommandException;
import com.gitlab.survivalRaiz.core.messages.Message;
import com.gitlab.survivalRaiz.landProtection.LandProtection;
import com.gitlab.survivalRaiz.landProtection.terrain.Terrain;
import com.gitlab.survivalRaiz.permissions.management.Perms;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.UUID;

public class TransferCmd extends ConfigCommand {
    final LandProtection plugin;

    public TransferCmd(LandProtection plugin) {
        super("/transferir-terreno", "dá o terreno a um jogador", "/transferir-terreno <novo dono>",
                new ArrayList<>(), "/transferir-terreno");
        this.plugin = plugin;
    }

    @Override
    public int validate(CommandSender commandSender, String s, String[] strings) throws CommandException {
        if (!(commandSender instanceof Player))
            throw new SRCommandException(commandSender, Message.PLAYER_ONLY_COMMAND, plugin.getCore());

        final Player p = (Player) commandSender;

        if (strings.length < 2)
            throw new SRCommandException(p, Message.SYNTAX, getUsage(), plugin.getCore());

        final int id = Integer.parseInt(strings[0]);
        final Terrain t = plugin.getLandManager().fromDb(id);

        if (t == null)
            throw new SRCommandException(p, Message.TERRAIN_DOES_NOT_EXIST, plugin.getCore());

        if (!t.getOwner().equals(p.getUniqueId()) && !Perms.GIVE_ALL_TERRAINS.allows(p))
            throw new SRCommandException(p, Message.TERRAIN_IS_NOT_YOURS, plugin.getCore());

        if (!Bukkit.getOfflinePlayer(UUID.fromString(strings[1])).hasPlayedBefore())
            throw new SRCommandException(p, Message.PLAYER_NOT_FOUND, plugin.getCore());

        if (plugin.getLandManager().isDue(plugin, t))
            throw new SRCommandException(p, Message.TERRAIN_TAX_FRAUD, plugin.getCore());

        final LocalDate now = LocalDate.now();
        final LocalDate due = plugin.getLandManager().getDueDate(t.getId());
        final int threshold = plugin.getCostsConfig().getTimeThreshold();

        if (due.minusDays(threshold).isBefore(now))
            throw new SRCommandException(p, Message.PAY_NEXT_TAX, plugin.getCore());

        return 0;
    }

    @Override
    public void run(CommandSender commandSender, String s, String[] strings) throws CommandException {
        validate(commandSender, s, strings);

        final int id = Integer.parseInt(strings[0]);
        final Terrain t = plugin.getLandManager().fromDb(id);
        final OfflinePlayer owner = Bukkit.getOfflinePlayer(UUID.fromString(strings[1]));

        t.setOwner(owner.getUniqueId());
        plugin.getCore().getDbManager().updateDueDate(id);
    }
}
