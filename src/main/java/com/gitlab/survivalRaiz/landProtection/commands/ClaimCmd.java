package com.gitlab.survivalRaiz.landProtection.commands;

import api.skwead.commands.ConfigCommand;
import api.skwead.exceptions.exceptions.CommandException;
import com.gitlab.survivalRaiz.core.excepions.SRCommandException;
import com.gitlab.survivalRaiz.core.messages.Message;
import com.gitlab.survivalRaiz.landProtection.LandProtection;
import com.gitlab.survivalRaiz.landProtection.terrain.Terrain;
import com.gitlab.survivalRaiz.permissions.management.Perms;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Collections;

public class ClaimCmd extends ConfigCommand {

    private final LandProtection plugin;

    public ClaimCmd(LandProtection plugin) {
        super("comprar", "compra terreno centrado na posição do jogador",
                "/comprar [raio]", Collections.singletonList("tc"), "comprar");
        this.plugin = plugin;
    }

    @Override
    public int validate(CommandSender commandSender, String s, String[] strings) throws CommandException {
        if (!(commandSender instanceof Player))
            throw new SRCommandException(commandSender, Message.PLAYER_ONLY_COMMAND, plugin.getCore());

        final Player p = (Player) commandSender;

        final int x = p.getLocation().getChunk().getX();
        final int y = p.getLocation().getChunk().getZ();

        if (this.plugin.getLandManager().isTerrain(x, y))
            throw new SRCommandException(p, Message.CHUNK_ALREADY_CLAIMED, plugin.getCore());

        if (strings.length > 0)
            try {
                final int r = Integer.parseInt(strings[0]);

                for (int i = x - r; i < x + r; i++)
                    for (int j = x + r; j < y + r; j++)
                        if (this.plugin.getLandManager().isTerrain(i, j))
                            throw new SRCommandException(p, Message.CHUNK_ALREADY_CLAIMED, plugin.getCore());

                return 1;
            } catch (NumberFormatException e) {
                throw new SRCommandException(commandSender, Message.SYNTAX, getUsage(), plugin.getCore());
            }

        return 0;
    }

    @Override
    public void run(CommandSender commandSender, String s, String[] strings) throws CommandException {
        final int syntax = validate(commandSender, s, strings);
        final Player p = (Player) commandSender;
        final int r = syntax == 0 ? 0 : Integer.parseInt(strings[0]);
        final double price = plugin.getCostsConfig().getChunkPrice() * Math.pow((2*r)+1, 2);
        final double balance = plugin.getEconomyPlugin().getEconomy().getBalance(p.getUniqueId());

        if (balance < price && !Perms.SUPER_WALLET.allows(p))
            throw new SRCommandException(p, Message.PLAYER_BROKE, plugin.getCore());

        final Terrain t = new Terrain(
                p.getLocation().getChunk().getX(), p.getLocation().getChunk().getZ(), p.getUniqueId(), r);

        if (!Perms.SUPER_WALLET.allows(p))
            plugin.getEconomyPlugin().getEconomy().withdrawPlayer(p.getUniqueId(), price);

        this.plugin.getLandManager().register(t);
    }
}
