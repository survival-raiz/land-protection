package com.gitlab.survivalRaiz.landProtection.commands;

import api.skwead.commands.ConfigCommand;
import api.skwead.exceptions.exceptions.CommandException;
import com.gitlab.survivalRaiz.core.excepions.SRCommandException;
import com.gitlab.survivalRaiz.core.messages.Message;
import com.gitlab.survivalRaiz.landProtection.LandProtection;
import com.gitlab.survivalRaiz.landProtection.terrain.Terrain;
import com.gitlab.survivalRaiz.permissions.management.Perms;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Set;

public class ListDueTerrainsCmd extends ConfigCommand {

    private final LandProtection plugin;

    public ListDueTerrainsCmd(LandProtection plugin) {
        super("corretoria", "Mostra os terrenos disponíveis para venda",
                "/corretoria [(visitar|comprar) <n>]", new ArrayList<>(), "corretoria");

        this.plugin = plugin;
    }

    /**
     * 0 -> list
     * 1 -> visit
     * 2 -> buy
     *
     * @param commandSender
     * @param s
     * @param strings
     * @return
     * @throws CommandException
     */
    @Override
    public int validate(CommandSender commandSender, String s, String[] strings) throws CommandException {
        if (strings.length == 0)
            return 0;

        if (!(commandSender instanceof Player))
            throw new SRCommandException(commandSender, Message.PLAYER_ONLY_COMMAND, plugin.getCore());

        if (strings.length < 2)
            throw new SRCommandException(commandSender, Message.SYNTAX, plugin.getCore());

        if (Integer.parseInt(strings[1]) > plugin.getLandManager().getCount())
            throw new SRCommandException(commandSender, Message.TERRAIN_DOES_NOT_EXIST, plugin.getCore());

        if (strings[0].equals("visitar"))
            return 1;

        if (strings[0].equals("comprar")) {
            final int r = plugin.getLandManager().fromDb(Integer.parseInt(strings[1])).getR();
            final double price = plugin.getCostsConfig().getChunkPrice() * Math.pow((2 * r) + 1, 2);

            if (Perms.SR.allows((Player) commandSender) ||
                    plugin.getEconomyPlugin().getEconomy().has(((Player) commandSender).getUniqueId(), price / 2))
                return 2;

            throw new SRCommandException(commandSender, Message.PLAYER_BROKE, plugin.getCore());
        }

        throw new SRCommandException(commandSender, Message.SYNTAX, plugin.getCore());
    }

    @Override
    public void run(CommandSender commandSender, String s, String[] strings) throws CommandException {
        int syntax = validate(commandSender, s, strings);

        if (syntax == 0) {
            final Set<Terrain> terrains = plugin.getLandManager().getDue();

            plugin.getCore().getMsgHandler().message(commandSender, Message.AUCTION_TERRAIN_LIST_HEADER);

            terrains.forEach(t ->
                    plugin.getCore().getMsgHandler().message(commandSender, Message.AUCTION_TERRAIN_LIST_ELEMENT,
                            s1 -> s1.replaceAll("%id", String.valueOf(t.getId()))
                                    .replaceAll("%coords%", String.format("[%d,%d]", t.getX(), t.getY()))
                                    .replaceAll("%owner%", Bukkit.getOfflinePlayer(t.getOwner()).getName()))
            );
        }

        final Player p = (Player) commandSender;

        if (syntax == 1) {
            // TODO: 12/31/21 this
            return;
        }

        if (syntax == 2) {
            final Terrain t = plugin.getLandManager().fromDb(Integer.parseInt(strings[1]));
            final int r = t.getR();
            final double price = plugin.getCostsConfig().getChunkPrice() * Math.pow((2 * r) + 1, 2);

            if (!Perms.SUPER_WALLET.allows(p))
                plugin.getEconomyPlugin().getEconomy().withdrawPlayer(p.getUniqueId(), price / 2);

            t.setOwner(p.getUniqueId());
        }
    }
}
